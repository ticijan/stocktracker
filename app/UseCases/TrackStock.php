<?php

namespace App\UseCases;

use App\Events\NowInStock;
use App\Models\History;
use App\Models\Stock;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TrackStock implements ShouldQueue
{
    use Dispatchable;

    protected $stock;

    protected $status;

    public function __construct(Stock $stock)
    {
        $this->stock = $stock;
    }

    public function handle()
    {
        $this->checkAvailability();
        $this->notifyUser();
        $this->refreshStock();
        $this->recordHistory();
    }

    protected function checkAvailability()
    {
        $this->status = $this->stock
            ->retailer
            ->client()
            ->checkAvailability($this->stock);
    }

    protected function notifyUser()
    {
        if ($this->isNowAvailable()) {
            event(new NowInStock($this->stock));
        }
    }

    protected function refreshStock()
    {
        $this->stock->update([
            'is_available' => $this->status->getIsAvailable(),
            'price' => $this->status->getPrice()
        ]);
    }

    protected function recordHistory()
    {
        History::create([
            'price' => $this->stock->price,
            'is_available' => $this->stock->is_available,
            'stock_id' => $this->stock->id,
            'product_id' => $this->stock->product_id
        ]);
    }

    private function isNowAvailable()
    {
        return ! $this->stock->is_available && $this->status->getIsAvailable();
    }
}
