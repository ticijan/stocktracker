<?php

namespace App\Clients\ValueObject;

class ClientResponse
{
    /** @var bool */
    private $is_available;

    /** @var float */
    private $price;

    public function __construct(bool $is_available, int $price)
    {
        $this->is_available = $is_available;
        $this->price = $price;
    }

    public function getIsAvailable(): bool
    {
        return $this->is_available;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}
