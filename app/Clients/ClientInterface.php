<?php

namespace App\Clients;

use App\Clients\ValueObject\ClientResponse;
use App\Models\Stock;

interface ClientInterface
{
    /**
     * @param Stock $stock
     * @return ClientResponse
     */
    public function checkAvailability(Stock $stock): ClientResponse;
}
