<?php

namespace App\Clients;

use App\Clients\ValueObject\ClientResponse;
use App\Models\Stock;
use Illuminate\Support\Facades\Http;

class BestBuy implements ClientInterface
{
    /** @inheritDoc */
    public function checkAvailability(Stock $stock): ClientResponse
    {
        $response = Http::get($this->getEndpoint($stock->getSku()))->json();

        return new ClientResponse(
            $response['onlineAvailability'],
            $this->dollarsToCents($response['salePrice'])
        );
    }

    private function getEndpoint(string $sku): string
    {
        $apiKey = config('services.clients.bestBuy.key');

        return "https://api.bestbuy.com/v1/products/{$sku}.json?apiKey={$apiKey}";
    }

    private function dollarsToCents($dollars)
    {
        return (int) ($dollars * 100);
    }
}
