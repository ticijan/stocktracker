<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function track()
    {
        $this->stock->each->track();
    }

    public function isAvailable()
    {
        return $this->stock()->where('is_available', true)->exists();
    }

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }

    public function history()
    {
        return $this->hasMany(History::class);
    }
}
