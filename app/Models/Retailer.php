<?php

namespace App\Models;

use App\ClassTransformers\ClientTransformer;
use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    protected $guarded = [];

    public function getName()
    {
        return $this->name;
    }

    public function client()
    {
        return (new ClientTransformer())->make($this);
    }

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }

    public function addStock(Product $product, Stock $stock)
    {
        $stock->product_id = $product->id;

        $this->stock()->save($stock);
    }
}
