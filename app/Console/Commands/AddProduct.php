<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Retailer;
use App\Models\Stock;
use Illuminate\Console\Command;

class AddProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new product for tracking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $retailers = Retailer::select('id', 'name')->get()->toArray();
        $retailers = $this->formatRetailers($retailers);

        $productName = $this->ask('What is the name of the product?');

        $retailerName = $this->choice(
            'What is the name of the retailer?',
            $retailers
        );

        $retailerId = $this->extractRetailerId($retailerName);

        $url = $this->ask('Insert the product URL:');

        $sku = $this->ask('Insert the product SKU:');

        $isAvailable = $this->confirm('Is the product available right now?');

        $this->line('Thank you for the input, wait a bit until the process is finished...');

        if ($this->addProduct($productName, $retailerId, $url, $sku, $isAvailable)) {
            $this->info('The product was sucessfuly added, run the track command to get the updated information you need.');
        } else {
            $this->error('The information was not valid.');
        }
    }

    private function formatRetailers(array $retailers): array
    {
        $formatted = [];

        foreach ($retailers as $retailer) {
            $formatted[] = $retailer['name'] . '-' . $retailer['id'];
        }

        return $formatted;
    }

    private function extractRetailerId(string $retailer): int
    {
        $id = explode('-', $retailer);

        return (int) end($id);
    }

    private function addProduct(string $productName, int $retailerId, string $url, string $sku, bool $isAvailable)
    {
        try {
            $product = Product::create(['name' => $productName]);

            $retailer = Retailer::find($retailerId);

            $retailer->addStock($product, new Stock([
                'price' => 0,
                'url' => $url,
                'sku' => $sku,
                'is_available' => $isAvailable
            ]));

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
