<?php

namespace App\ClassTransformers;

use App\Exceptions\ClientNotFoundException;
use App\Models\Retailer;
use Illuminate\Support\Str;

class ClientTransformer
{
    const CLIENTS_CLASS_PATH = "App\\Clients\\";

    public function make(Retailer $retailer)
    {
        $class = self::CLIENTS_CLASS_PATH . Str::studly($retailer->getName());

        if (!class_exists($class)) {
            throw new ClientNotFoundException('Client not found for ' . $retailer->getName());
        }

        return new $class;
    }
}
