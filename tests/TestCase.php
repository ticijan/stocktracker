<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Http;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function mockClientRequest($available = true, $price = 299.99)
    {
        Http::fake([
            'https://api.bestbuy.com/*' => Http::response(['salePrice' => $price, 'onlineAvailability' => $available])
        ]);
    }
}
