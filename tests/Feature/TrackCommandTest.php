<?php

namespace Tests\Feature;

use App\Models\Product;
use Tests\TestCase;
use RetailerProductUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrackCommandTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_tracks_product_stock()
    {
        $this->seed(RetailerProductUserSeeder::class);

        $this->assertFalse(Product::first()->isAvailable());

        $this->mockClientRequest();

        $this->artisan('track');

        $this->assertTrue(Product::first()->isAvailable());
    }
}
