<?php

namespace Tests\Clients;

use App\Models\Stock;
use App\Clients\BestBuy;
use Tests\TestCase;
use RetailerProductUserSeeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * @group api
 */
class BestBuyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_tracks_a_product()
    {
        $this->seed(RetailerProductUserSeeder::class);

        $stock = tap(Stock::first())->update([
            'sku' => '6364253',
            'url' => 'https://www.bestbuy.com/site/nintendo-switch-32gb-console-gray-joy-con/6364253.p?skuId=6364253'
        ]);

        try {
            (new BestBuy())->checkAvailability($stock);
        } catch (\Exception $e) {
            $this->fail('Failed to track the BestBuy API properly. ' . $e->getMessage());
        }

        $this->assertTrue(true);
    }

    /** @test */
    function it_creates_the_proper_stock_status_response()
    {
        Http::fake([
            'https://api.bestbuy.com/*' => Http::response(['salePrice' => 299.99, 'onlineAvailability' => true])
        ]);

        $stock = new Stock();
        $stock->sku = '23543';
        $stock->is_available = false;

        $stockStatus = (new BestBuy())->checkAvailability($stock);

        $this->assertEquals(29999, $stockStatus->getPrice());
        $this->assertTrue($stockStatus->getIsAvailable());
    }
}
