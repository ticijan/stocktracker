<?php

namespace Tests\Unit;

use App\Models\Product;
use Tests\TestCase;
use RetailerProductUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductHistoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_records_history_when_a_product_is_tracked()
    {
        $this->seed(RetailerProductUserSeeder::class);

        $this->mockClientRequest($available = true, $price = 99.00);

        $product = tap(Product::first(), function ($product) {
            $this->assertCount(0, $product->history);

            $product->track();

            $this->assertCount(1, $product->refresh()->history);
        });

        $history = $product->history->first();
        $this->assertEquals($price * 100, $history->price);
        $this->assertEquals($available, $history->is_available);
        $this->assertEquals($product->stock[0]->id, $history->stock_id);
    }
}
