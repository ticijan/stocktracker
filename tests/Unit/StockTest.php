<?php

namespace Tests\Unit;

use App\Exceptions\ClientNotFoundException;
use App\Models\Stock;
use App\Models\Retailer;
use Tests\TestCase;
use RetailerProductUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StockTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_throws_an_exception_if_a_client_is_not_found_when_tracking()
    {
        $this->seed(RetailerProductUserSeeder::class);

        Retailer::first()->update(['name' => 'Foo Retailer']);

        $this->expectException(ClientNotFoundException::class);

        Stock::first()->track();
    }
}

