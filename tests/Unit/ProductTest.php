<?php

namespace Tests\Unit;

use App\Models\Product;
use Tests\TestCase;
use RetailerProductUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_checks_stock_for_products_at_retailers()
    {
        $this->seed(RetailerProductUserSeeder::class);

        tap(Product::first(), function ($product) {
            $this->assertFalse($product->isAvailable());

            $product->stock()->first()->update(['is_available' => true]);

            $this->assertTrue($product->isAvailable());
        });

    }
}
