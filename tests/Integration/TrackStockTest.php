<?php

namespace Tests\Integration;

use App\Models\Stock;
use App\Models\History;
use App\UseCases\TrackStock;
use RetailerProductUserSeeder;
use Tests\TestCase;
use App\Notifications\StockUpdate;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrackStockTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Notification::fake();

        $this->mockClientRequest($available = true, $price = 249.00);

        $this->seed(RetailerProductUserSeeder::class);

        (new TrackStock(Stock::first()))->handle();
    }

    /** @test */
    function it_notifies_the_user()
    {
        Notification::assertTimesSent(1, StockUpdate::class);
    }

    /** @test */
    function it_refreshes_the_local_stock()
    {
        $this->assertDatabaseHas('stocks', [
            'price' => 24900,
            'is_available' => true
        ]);
    }

    /** @test */
    function it_records_to_history()
    {
        $this->assertEquals(1, History::count());
    }
}
