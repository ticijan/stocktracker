<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/email-preview', function () {
    $user = factory(\App\Models\User::class)->create();

    return (new \App\Notifications\StockUpdate(\App\Models\Stock::first()))->toMail($user);
});
